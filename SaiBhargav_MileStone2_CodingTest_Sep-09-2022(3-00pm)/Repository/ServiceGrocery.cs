﻿using SaiBhargav_MileStone2_CodingTest_Sep_09_2022_3_00pm_.Entities;
using SaiBhargav_MileStone2_CodingTest_Sep_09_2022_3_00pm_.Persistance;
using System.Collections.Generic;
using System.Linq;

namespace SaiBhargav_MileStone2_CodingTest_Sep_09_2022_3_00pm_.Repository
{
    public class ServiceGrocery : IServicesGrocery
    {
        private Grocery_DbContext _done;
        public ServiceGrocery(Grocery_DbContext done)
        {
            _done = done;
        }
        public List<Category> GetAlloftheCatrgories()
        {
            return _done.Category.ToList();

        }

        public List<Product> GetProducts()
        {
            return _done.Product.ToList();
        }

        public List<ProductOrder> GetOrderInfo(int userId)
        {
            _done.ProductOrder.SingleOrDefault(first => first.Userid == userId);
            return _done.ProductOrder.ToList();
        }

        public Product GetProductById(int id)
        {
            return _done.Product.SingleOrDefault(second=> second.Productid == id);
        }

        public ProductOrder PlaceOrder(int Productid, int UserId)
        {
            var orderid = _done.ProductOrder.Max(first => first.Orderid) + 1;
            var product = _done.Product.SingleOrDefault(second => second.Productid == Productid);
            var user = _done.ApplicationUser.SingleOrDefault(third=> third.Id == UserId);
            var productorder = new ProductOrder()
            {
                Userid = UserId,
                Productid = Productid,
                Orderid = orderid,
                Product = product,
                User = user
            };
            _done.ProductOrder.Add(productorder);
            _done.SaveChanges();
            return productorder;
        }
    }
}
