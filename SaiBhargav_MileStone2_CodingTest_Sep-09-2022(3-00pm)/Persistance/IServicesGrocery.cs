﻿using SaiBhargav_MileStone2_CodingTest_Sep_09_2022_3_00pm_.Entities;
using System.Collections.Generic;

namespace SaiBhargav_MileStone2_CodingTest_Sep_09_2022_3_00pm_.Persistance
{
    public interface IServicesGrocery
    {
        public List<Product> GetProducts();
        public List<Category> GetAlloftheCatrgories();
        public ProductOrder PlaceOrder(int Id, int UserId);


        public Product GetProductById(int id);
        public List<ProductOrder> GetOrderInfo(int userId);
    }
}
