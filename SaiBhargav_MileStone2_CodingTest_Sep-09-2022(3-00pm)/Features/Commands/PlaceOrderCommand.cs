﻿using SaiBhargav_MileStone2_CodingTest_Sep_09_2022_3_00pm_.Entities;
using MediatR;


namespace SaiBhargav_MileStone2_CodingTest_Sep_09_2022_3_00pm_.Features.Commands
{
    public class PlaceOrderCommand : IRequest<ProductOrder>
    {
        public int ProductId { get; set; }
        public int UserId { get; set; }


    }
}
