﻿using MediatR;
using SaiBhargav_MileStone2_CodingTest_Sep_09_2022_3_00pm_.Entities;
using System.Collections.Generic;

namespace SaiBhargav_MileStone2_CodingTest_Sep_09_2022_3_00pm_.Features.Queries
{
    public class GetOrderInfoQueries : IRequest<List<ProductOrder>>
    {
        public int id { get; set; }
    }
}

