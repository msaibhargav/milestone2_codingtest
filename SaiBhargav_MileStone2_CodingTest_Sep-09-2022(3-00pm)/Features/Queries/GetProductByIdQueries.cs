﻿using MediatR;
using SaiBhargav_MileStone2_CodingTest_Sep_09_2022_3_00pm_.Entities;

namespace SaiBhargav_MileStone2_CodingTest_Sep_09_2022_3_00pm_.Features.Queries
{
    public class GetProductByIdQueries : IRequest<Product>

    {
        public int id { get; set; }
    }
}