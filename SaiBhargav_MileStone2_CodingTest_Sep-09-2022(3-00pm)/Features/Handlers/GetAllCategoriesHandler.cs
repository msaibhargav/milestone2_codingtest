﻿using MediatR;
using SaiBhargav_MileStone2_CodingTest_Sep_09_2022_3_00pm_.Entities;
using SaiBhargav_MileStone2_CodingTest_Sep_09_2022_3_00pm_.Features.Queries;
using SaiBhargav_MileStone2_CodingTest_Sep_09_2022_3_00pm_.Persistance;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Threading;

namespace SaiBhargav_MileStone2_CodingTest_Sep_09_2022_3_00pm_.Features.Handlers
{
    public class GetAllCategoriesHandler : IRequestHandler<GetAllCategoriesQueries, List<Category>>
    {
        private readonly IServicesGrocery groceryService;
        public GetAllCategoriesHandler(IServicesGrocery groceryService)
        {
            this.groceryService = groceryService;
        }
        public async Task<List<Category>> Handle(GetAllCategoriesQueries request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(groceryService.GetAlloftheCatrgories());

        }
    }
}
