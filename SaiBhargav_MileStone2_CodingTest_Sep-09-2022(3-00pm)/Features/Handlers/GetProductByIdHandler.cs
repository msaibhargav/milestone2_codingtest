﻿using MediatR;
using SaiBhargav_MileStone2_CodingTest_Sep_09_2022_3_00pm_.Entities;
using SaiBhargav_MileStone2_CodingTest_Sep_09_2022_3_00pm_.Features.Queries;
using SaiBhargav_MileStone2_CodingTest_Sep_09_2022_3_00pm_.Persistance;
using System.Threading.Tasks;
using System.Threading;

namespace SaiBhargav_MileStone2_CodingTest_Sep_09_2022_3_00pm_.Features.Handlers
{
    public class GetProductByIdHandler : IRequestHandler<GetProductByIdQueries, Product>
    {
        private readonly IServicesGrocery groceryService;
        public GetProductByIdHandler(IServicesGrocery groceryService)
        {
            this.groceryService = groceryService;
        }

        public async Task<Product> Handle(GetProductByIdQueries request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(groceryService.GetProductById(request.id));
        }
    }
}
