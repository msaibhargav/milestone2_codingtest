﻿using MediatR;
using SaiBhargav_MileStone2_CodingTest_Sep_09_2022_3_00pm_.Entities;
using SaiBhargav_MileStone2_CodingTest_Sep_09_2022_3_00pm_.Features.Commands;
using SaiBhargav_MileStone2_CodingTest_Sep_09_2022_3_00pm_.Persistance;
using System.Threading.Tasks;
using System.Threading;

namespace SaiBhargav_MileStone2_CodingTest_Sep_09_2022_3_00pm_.Features.Handlers
{
    public class PlaceOrderHandler : IRequestHandler<PlaceOrderCommand, ProductOrder>
    {
        private readonly IServicesGrocery _sai;

        public PlaceOrderHandler(IServicesGrocery sai)
        {
            _sai = sai;
        }

        public async Task<ProductOrder> Handle(PlaceOrderCommand request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_sai.PlaceOrder(request.UserId, request.ProductId));
        }
    }
}
