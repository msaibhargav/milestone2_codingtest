using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using SaiBhargav_MileStone2_CodingTest_Sep_09_2022_3_00pm_.Entities;
using SaiBhargav_MileStone2_CodingTest_Sep_09_2022_3_00pm_.Persistance;
using SaiBhargav_MileStone2_CodingTest_Sep_09_2022_3_00pm_.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SaiBhargav_MileStone2_CodingTest_Sep_09_2022_3_00pm_
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddControllers().AddNewtonsoftJson(x => x.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);

            services.AddDbContext<Grocery_DbContext>(item => item.UseSqlServer(Configuration.GetConnectionString("DefaultDatabase")));

            services.AddMvc();
            services.AddTransient<IServicesGrocery, ServiceGrocery>();
            services.AddMediatR(typeof(Startup).Assembly);
            services.AddSwaggerGen(c =>

            {

                c.SwaggerDoc("v1.0", new Microsoft.OpenApi.Models.OpenApiInfo
                { Version = "v1", Title = "Sai_Coding_Test", Description = "Questions" });

            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseSwagger();

            app.UseSwaggerUI(c =>

            {

                c.SwaggerEndpoint("./v1.0/swagger.json", "Sai_Coding");


            });

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
