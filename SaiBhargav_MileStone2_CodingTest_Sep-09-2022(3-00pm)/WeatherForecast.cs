using System;

namespace SaiBhargav_MileStone2_CodingTest_Sep_09_2022_3_00pm_
{
    public class WeatherForecast
    {
        public DateTime Date { get; set; }

        public int TemperatureC { get; set; }

        public int TemperatureF => 32 + (int)(TemperatureC / 0.5556);

        public string Summary { get; set; }
    }
}
