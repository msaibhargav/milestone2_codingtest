﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SaiBhargav_MileStone2_CodingTest_Sep_09_2022_3_00pm_.Entities;
using SaiBhargav_MileStone2_CodingTest_Sep_09_2022_3_00pm_.Features.Commands;
using SaiBhargav_MileStone2_CodingTest_Sep_09_2022_3_00pm_.Features.Queries;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SaiBhargav_MileStone2_CodingTest_Sep_09_2022_3_00pm_.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class GroceryController : ControllerBase
    {
        private readonly ISender _sender;



        public GroceryController(ISender sender)
        {
            _sender = sender;
        }



        [HttpGet]
        public async Task<IEnumerable<Product>> ViewAllProducts()
        {
            return await _sender.Send(new GetAllProductQueries());
        }



        [HttpGet]
        public async Task<IEnumerable<Category>> ViewListOfCategories()
        {
            return await _sender.Send(new GetAllCategoriesQueries());
        }



        [HttpGet]
        public async Task<Product> GetProductDetailsById(int productid)
        {
            return await _sender.Send(new GetProductByIdQueries() { id = productid });
        }

        [HttpGet]
        public async Task<IEnumerable<ProductOrder>> ShowOrderInfo(int userid)
        {
            return await _sender.Send(new GetOrderInfoQueries() { id = userid });
        }

        [HttpPost("addproduct")]
        public async Task<ProductOrder> AddProductOrder(int productid, int userid)
        {
            return await _sender.Send(new PlaceOrderCommand() { ProductId = productid, UserId = userid });
        }

    }
}
