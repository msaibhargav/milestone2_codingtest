﻿using System;
using System.Collections.Generic;

namespace SaiBhargav_MileStone2_CodingTest_Sep_09_2022_3_00pm_.Entities
{
    public partial class Category
    {
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
    }
}
