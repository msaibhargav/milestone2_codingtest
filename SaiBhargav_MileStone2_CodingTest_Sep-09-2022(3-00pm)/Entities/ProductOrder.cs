﻿using System;
using System.Collections.Generic;

namespace SaiBhargav_MileStone2_CodingTest_Sep_09_2022_3_00pm_.Entities
{
    public partial class ProductOrder
    {
        public int Orderid { get; set; }
        public int Productid { get; set; }
        public int Userid { get; set; }

        public virtual Product Product { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
}
